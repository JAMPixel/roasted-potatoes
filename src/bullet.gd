extends KinematicBody2D

const BULLET_SPEED = 800
const DAMAGE = 200

var speed = BULLET_SPEED

onready var damage = DAMAGE
var direction = Vector2(0, -1)

func garbage():
	queue_free()

func _physics_process(delta):
	var collision_info = move_and_collide(speed * delta * direction)
	if collision_info:
		print("collision")
		var collider = collision_info.collider
		if collider.has_method("hit"):
			collider.hit(damage)
		garbage()
	elif is_out():
		garbage()

func set_direction(d):
	direction = d
	
func is_out():
	return position.y < 0 or position.y > 1080 or position.x < 0 or position.y > 1920

