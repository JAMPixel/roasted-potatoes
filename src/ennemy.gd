extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const SHIP_SPEED = 200
const FIRE_SPEED = 2
const LIFE = 200

onready var speed = SHIP_SPEED
onready var direction = Vector2(0, 0)
onready var life = LIFE
onready var destroyed = false

onready var Bullet = preload("res://bullet_ennemy.tscn")

var shoot_tick = 0
onready var bullet_space


func default_move(object, delta):
	object.position.y += (speed*delta)

func default_garbage(object):
	queue_free()

var move_strategy = funcref(self, "default_move")
var garbage_strategy = funcref(self, "default_garbage")

func _ready():
	get_node("animation").play("hit")

func init(move_strategy, gargbage_strategy, _bullet_space):
	self.move_strategy = move_strategy
	self.garbage_strategy = garbage_strategy
	self.bullet_space = _bullet_space

func garbage():
	self.garbage_strategy.call_func(self)

func destroy():
	destroyed = true
	get_node("CollisionShape2D").queue_free()
	get_node("boom_sound").play()
	get_node("animation").play("destroy")
	
func shoot(delta):
	
	if not destroyed:
		shoot_tick += delta
		if position.y > 0 and shoot_tick > FIRE_SPEED:
			shoot_tick = 0
			var bullet = Bullet.instance()
			bullet.position = Vector2(global_position.x, global_position.y)
			bullet.direction = Vector2(cos(global_rotation - PI/2), sin(global_rotation - PI/2))
			bullet.speed = 250
			bullet_space.add_child(bullet)
	
func hit(damage):
	if (not destroyed):
		life -= damage
		get_node("animation").play("hit")
		get_node("hit_sound").play()
		if life <= 0:
			destroy()
	
func _physics_process(delta):
	self.move_strategy.call_func(self, delta)
	shoot(delta)
	


