extends Node


onready var ennemy_red = preload("res://ennemy_red.tscn")

const curved_paths = ["path_1", "path_2"]

const left_paths = ["l_path_1", "l_path_2", "l_path_3"]
const middle_paths = ["m_path_1", "m_path_2", "m_path_3"]
const right_paths = ["r_path_1", "r_path_2", "r_path_3"]



func spawn_right():
	for path in right_paths:
		spawn_one(ennemy_red, path)

func spawn_left():
	for path in left_paths:
		spawn_one(ennemy_red, path)

func spawn_middle():
	for path in middle_paths:
		spawn_one(ennemy_red, path)

func spawn_curved():
	for path in curved_paths:
		spawn_one(ennemy_red, path)

var spawn_tick = 0
var tick = 0

var steps = [
	{
		"begin": 0,
		"iteration": 0,
		"max_iteration": 10,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_curved")
		]
	}, 	{
		"begin": 4,
		"iteration": 0,
		"max_iteration": 10,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_left"),
			funcref(self, "spawn_right")
		]
	}, 	{
		"begin": 10,
		"iteration": 0,
		"max_iteration": 10,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_middle"),
			funcref(self, "spawn_curved")
		]
	}, 	{
		"begin": 15,
		"iteration": 0,
		"max_iteration": 5,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_left"),
			funcref(self, "spawn_right")
		]
	}, 	{
		"begin": 20,
		"iteration": 0,
		"max_iteration": 3,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_middle"),
			funcref(self, "spawn_left"),
			funcref(self, "spawn_right")
		]
	}, 	{
		"begin": 25,
		"iteration": 0,
		"max_iteration": 10,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_left"),
			funcref(self, "spawn_right")
		]
	}, 	{
		"begin": 30,
		"iteration": 0,
		"max_iteration": 10,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_curved")
		]
	}
	, 	{
		"begin": 35,
		"iteration": 0,
		"max_iteration": 10,
		"spawn_rate": .700,
		"spawn_tick": 0,
		"funcs": [
			funcref(self, "spawn_middle")
		]
	}
]

export(float) var SPEED = 100
export(float) var spawn_rate = 3

onready var bullet_space = get_node("..")

func ennemy_move_strategy(object, delta):
	var parent = object.get_parent()
	var offset = parent.get_offset() + SPEED * delta
	parent.set_offset(offset) 
	# Remove if the object is out the screen
	if(parent.position.y > 1100):
		object.garbage()

func ennemy_garbage(object):
	object.get_parent().queue_free()

func spawn_one(to_instance, path_targeted):
	var ennemy = to_instance.instance()
	var path_follow = PathFollow2D.new()
	ennemy.init(
		funcref(self, "ennemy_move_strategy"),
		funcref(self, "ennemy_garbage_strategy"),
		bullet_space
	)
	path_follow.add_child(ennemy)
	get_node(path_targeted).add_child(path_follow)

func _process(delta):
	tick += delta
	for step in steps:
		step["spawn_tick"] += delta
		if 	step["spawn_tick"] > step["spawn_rate"] and step["begin"] < tick and step["iteration"] < step["max_iteration"]:
			step["spawn_tick"] = 0
			step["iteration"] += 1
			for to_call in step["funcs"]:
				to_call.call_func()