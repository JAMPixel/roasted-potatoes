extends Node

onready var ennemy_red = preload("res://ennemy_red.tscn")

var tick = 0

func _ready():
	get_node("player").connect("player_hit", self, "update_hud")

func _process(delta):
	pass
	
func update_hud(life_ratio):
	get_node("hud").update_life(life_ratio)