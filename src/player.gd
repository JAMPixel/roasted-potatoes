extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const SHIP_SPEED = 500
const FIRE_SPEED = 0.300

export var speed = SHIP_SPEED
export var fire_speed = FIRE_SPEED
export var life = 500
export var max_life = 500

onready var direction = Vector2(0, 0)
onready var Bullet_1 = preload("res://bullet.tscn")
var tick = 0

var last_reactor_angle = 0
var last_scale = 0

signal player_hit

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func compute_direction():
	var y_dir = 0
	
	if Input.is_action_pressed("player_right") and not(Input.is_action_pressed("player_left")):
		y_dir = 1
	elif Input.is_action_pressed("player_left") and not(Input.is_action_pressed("player_right")):
		y_dir = -1
	
	var x_dir = 0
	if Input.is_action_pressed("player_up") and not(Input.is_action_pressed("player_bottom")):
		x_dir = -1
	elif Input.is_action_pressed("player_bottom") and not(Input.is_action_pressed("player_up")):
		x_dir = 1
		
	return Vector2(y_dir, x_dir)
	
func move(delta):
	direction = compute_direction()
	move_and_collide(speed * delta * direction)

func shoot(delta):
	tick += delta
		
	if tick > fire_speed :
		if Input.is_action_pressed("player_shoot"):
			
			get_node("sound_shoot").play()
			tick = 0
			# Bullet instancing
			var bullet =Bullet_1.instance()
			bullet.position = Vector2(position.x + 10, position.y -30)
			get_node("..").add_child(bullet)
			
			var bullet2 =Bullet_1.instance()
			bullet2.position = Vector2(position.x - 10, position.y -30)
			get_node("..").add_child(bullet2)
			
			bullet = Bullet_1.instance()
			bullet.position = Vector2(position.x + 30, position.y )
			bullet.direction = Vector2(0.1, -1 )
			get_node("..").add_child(bullet)
			
			bullet = Bullet_1.instance()
			bullet.position = Vector2(position.x - 30, position.y )
			bullet.direction = Vector2(-0.1, -1 )
			get_node("..").add_child(bullet)
			
	if Input.is_action_just_released("player_shoot"):
		tick += fire_speed

func edit_reactor(delta):
	var reactor = get_node("reactor")
	
	var angle = 0 
	var scale = 0
	
	if direction.x == 0 and direction.y == 0:
		angle = atan2(0, -1)
	else:
		angle = atan2(-direction.x, direction.y - 0.8)
	while (angle < 0):
		angle += 2 * PI
		
	reactor.rotation =  angle * 0.1 + last_reactor_angle * 0.9
	last_reactor_angle = reactor.rotation
	
	scale = min(abs(direction.x) - direction.y, 1) * 2
	reactor.scale = Vector2(2, scale * 0.1 + last_scale * 0.9)
	last_scale = reactor.scale.y

func hit(damage):
	life -= damage
	get_node("sound_hit").play()
	emit_signal("player_hit", life / max_life)

func _physics_process(delta):
	shoot(delta)
	move(delta)
	edit_reactor(delta)
	pass